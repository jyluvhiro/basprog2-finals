#include <string>
#include <iostream>
#include <conio.h>
#include <time.h>
#include <conio.h>
#include <vector>
#include "pch.h"

//TO HAVE A CHECK UP ON POKEMON STATS
void checkpokemon();

//POKEMART STUFF
void PokeMart();

int PotPrice = 300;
int PBPrice = 200;
int PGBPrice = 600;
int PUBPrice = 1200;

//HEALING IN TOWN
void townhealpokemon();

//PLAYER CAN HEAL OUTSIDE TOWN BY POTION
void potionhealpokemon();

//BELOW IS THE CODES//

void checkpokemon()
{
	for (size_t i = 0; i < myPokemons.size(); i++)
	{
		cout << myPokemons[i]->NAME << " Status:" << endl;
		cout << "Level: " << myPokemons[i]->LVL << endl;
		cout << "HP: " << myPokemons[i]->CURHP << "/" << myPokemons[i]->MAXHP << endl;
		cout << "Damage: " << myPokemons[i]->ATK << endl;
		cout << "Exp: " << myPokemons[i]->XP << "/" << myPokemons[i]->XPTOLVLUP << endl;
		cout << "----------------------" << endl;
	}
}

void PokeMart()
{
	cout << "Hi, Welcome to PokeMart!" << endl;
	cout << "What would you like to purchase?" << endl;
	cout << "[B] PokeBall: P200   [G] GreatBall: P600   [U] UltraBall: P1200   [P] Potion: P300" << endl;

	char purchase;
	cin >> purchase;
	if (purchase == 'B' || purchase == 'b')
	{
		if (MONEY >= PBPrice)
		{
			MONEY = MONEY - PBPrice;
			InvPokeB++;
			cout << "Thank you for purchasing the item!" << endl;
		}
		else if (MONEY <= PBPrice)
		{
			cout << "It seems like you don't have enough money!" << endl;
		}
	}
	else if (purchase == 'G' || purchase == 'g')
	{
		if (MONEY >= PGBPrice)
		{
			MONEY = MONEY - PGBPrice;
			InvPokeGB++;
			cout << "Thank you for purchasing the item!" << endl;
		}
		else if (MONEY <= PGBPrice)
		{
			cout << "It seems like you don't have enough money!" << endl;
		}
	}
	else if (purchase == 'U' || purchase == 'u')
	{
		if (MONEY >= PUBPrice)
		{
			MONEY = MONEY - PUBPrice;
			InvPokeUB++;
			cout << "Thank you for purchasing the item!" << endl;
		}
		else if (MONEY <= PUBPrice)
		{
			cout << "It seems like you don't have enough money!" << endl;
		}
	}
	else if (purchase == 'P' || purchase == 'p')
	{
		if (MONEY >= PotPrice)
		{
			MONEY = MONEY - PotPrice;
			InvPotion++;
			cout << "Thank you for purchasing the item!" << endl;
		}
		else if (MONEY <= PotPrice)
		{
			cout << "It seems like you don't have enough money!" << endl;
		}
	}
}

void townhealpokemon()
{
	for (size_t i = 0; i < myPokemons.size(); i++)
	{
		myPokemons[i]->CURHP = 0;
		myPokemons[i]->CURHP = myPokemons[i]->CURHP + myPokemons[i]->MAXHP;
	}
}

void potionhealpokemon()
{
	for (size_t i = 0; i < myPokemons.size(); i++)
	{
		myPokemons[i]->CURHP + (myPokemons[i]->MAXHP / 2);
	}
}
