#include <string>
#include <iostream>
#include <conio.h>
#include <time.h>
#include <conio.h>
#include <vector>

using namespace std;

class WildPokemon
{
	// アクセシビリティ
	// public 以下は全部公開される
public:
	WildPokemon(string, string, int, int, int, int, int, int, int, int, int, int, int);

	string NAME;
	string ELEMENT;
	int MAXHP;
	int CURHP;
	int ATK;
	int DEX;
	int LVL;
	int XP;
	int XPTOLVLUP;
	int CURREV; //Current Evolve Status, 1=Base, 2=Evolved Once, 3=Evolved Thrice
	int EVOLVE; //This is the maximum Evolve number (Ex.Bulbasaur has 3 (Including Bulbasaur itself) evolutions)
	int POKEDEXNUM; //PokeDex Number
	int XPTOGIVE; //WILDPOKEMON EXP

	// 情報を出力する関数
	void print()
	{
		cout << "NAME:" << NAME << endl;
		cout << "ELEMENT: " << ELEMENT << endl;
		cout << "HP:" << MAXHP << endl;
		cout << "ATK:" << ATK << endl;
		cout << "DEX:" << DEX << endl;
		cout << "LEVEL: " << LVL << endl;
	}

	void LVLUP(vector<WildPokemon> WildPokemonsList);
};