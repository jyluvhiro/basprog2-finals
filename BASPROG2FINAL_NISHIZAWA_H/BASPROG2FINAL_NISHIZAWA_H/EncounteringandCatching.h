#include <iostream>
#include <conio.h>
#include <time.h>
#include <conio.h>
#include <vector>

#include "WildPokemonsList.h"

using namespace std;

int Chance;
string Rarity;

//MAP ACTIONS
void playermove();

string playerpos;
int playerx = 2;
int playerz = 2;

//GENERAL ACTIONS OF THE PLAYER, EX. FIGHT, CATCH, RUN AWAY
void Catching(WildPokemon * pokemon, string rarity, vector<WildPokemon> WildPokemonsList);

string Action;
int MONEY = 500;
int InvPotion = 0;

//WHAT HAVE PLAYER ENCOUNTERED & LVL OF THE POKEMON
WildPokemon *Encounter(string str, PokeDex* PokiPokie);

//CHANCE TO ENCOUNTER WILDPOKEMON
void wildpokemonencounter(PokeDex* PokiPokie);

//GET TO CHOOSE WHAT POKEMON BALL TO USE//
void ChoosePokeball();

int InvPokeB = 5;
int InvPokeGB = 0;
int InvPokeUB = 0;
//BELOW IS THE CODES//


WildPokemon *Encounter(string str, PokeDex* PokiPokie)
{
	int Dice1 = (rand() % 6) + 1;
	int Dice2 = (rand() % 6) + 1;
	if (Dice1 + Dice2 >= 8)
	{
		int RandomPokemon = rand() % PokiPokie->VeryCommon.size();
		WildPokemon *newpoke = PokiPokie->VeryCommon[RandomPokemon];
		newpoke->LVL = rand() % 9 + 1;
		if (newpoke->LVL >= 2)
		{
			newpoke->MAXHP = newpoke->MAXHP + (newpoke->MAXHP * newpoke->LVL * 0.15);
			newpoke->ATK = newpoke->ATK + (newpoke->ATK * newpoke->LVL * 0.10);
			newpoke->XPTOGIVE = newpoke->XPTOGIVE + (newpoke->XPTOGIVE * newpoke->LVL * 0.20);
		}
		return newpoke;
	}
	else if (Dice1 + Dice2 >= 5)
	{
		int RandomPokemon = rand() % PokiPokie->Common.size();
		WildPokemon *newpoke = PokiPokie->Common[RandomPokemon];
		newpoke->LVL = rand() % 9 + 1;
		if (newpoke->LVL >= 2)
		{
			newpoke->MAXHP = newpoke->MAXHP + (newpoke->MAXHP * newpoke->LVL * 0.15);
			newpoke->ATK = newpoke->ATK + (newpoke->ATK * newpoke->LVL * 0.10);
			newpoke->XPTOGIVE = newpoke->XPTOGIVE + (newpoke->XPTOGIVE * newpoke->LVL * 0.20);
		}
		return newpoke;
	}
	else if (Dice1 + Dice2 >= 3)
	{
		int RandomPokemon = rand() % PokiPokie->Rare.size();
		WildPokemon *newpoke = PokiPokie->Rare[RandomPokemon];
		newpoke->LVL = rand() % 9 + 1;
		if (newpoke->LVL >= 2)
		{
			newpoke->MAXHP = newpoke->MAXHP + (newpoke->MAXHP * newpoke->LVL * 0.15);
			newpoke->ATK = newpoke->ATK + (newpoke->ATK * newpoke->LVL * 0.10);
			newpoke->XPTOGIVE = newpoke->XPTOGIVE + (newpoke->XPTOGIVE * newpoke->LVL * 0.20);
		}
		return newpoke;
	}
	else if (Dice1 + Dice2 == 2)
	{
		int RandomPokemon = rand() % PokiPokie->VeryRare.size();
		WildPokemon *newpoke = PokiPokie->VeryRare[RandomPokemon];
		newpoke->LVL = rand() % 9 + 1;
		if (newpoke->LVL >= 2)
		{
			newpoke->MAXHP = newpoke->MAXHP + (newpoke->MAXHP * newpoke->LVL * 0.15);
			newpoke->ATK = newpoke->ATK + (newpoke->ATK * newpoke->LVL * 0.10);
			newpoke->XPTOGIVE = newpoke->XPTOGIVE + (newpoke->XPTOGIVE * newpoke->LVL * 0.20);
		}
		return newpoke;
	}
}

void wildpokemonencounter(PokeDex* PokiPokie)
{
	vector<WildPokemon> WildPokemonsList;

	int ExpandingChance = 15;
	int Chance = rand() % 100 + 1;
	if (Chance <= ExpandingChance)
	{
		Catching(Encounter("ASD", PokiPokie), Rarity, WildPokemonsList);
	}
}

void ChoosePokeball()
{
	if (InvPokeB >= 1 && InvPokeGB >= 1 && InvPokeUB >= 1)
	{
		cout << endl << "Choose a ball: " << endl << "[P] - Pokeball" << endl << "[G] - Great ball" << endl << "[U] - Ultra ball" << endl;
		cin >> Action;
		if (Action == "P")
		{
			cout << endl << "You threw a Pokeball" << endl;
			InvPokeB--;
			Chance += 10;
		}
		if (Action == "G")
		{
			cout << endl << "You threw a Great ball" << endl;
			InvPokeGB--;
			Chance += 20;
		}
		if (Action == "U")
		{
			cout << endl << "You threw an Ultra ball" << endl;
			InvPokeUB--;
			Chance += 30;
		}
	}
	else if (InvPokeB >= 1 && InvPokeGB == 0 && InvPokeUB == 0)
	{
		cout << endl << "Choose a ball: " << endl << "[P] - Pokebal" << endl;
		cin >> Action;
		if (Action == "P")
		{
			cout << endl << "You threw a Pokeball" << endl;
			InvPokeB--;
			Chance += 10;
		}
	}
	else if (InvPokeB == 0 && InvPokeGB >= 1 && InvPokeUB == 0)
	{
		cout << endl << "Choose a ball: " << endl << "[G] - Great ball" << endl;
		cin >> Action;
		if (Action == "G")
		{
			cout << endl << "You threw a Great ball" << endl;
			InvPokeGB--;
			Chance += 20;
		}
	}
	else if (InvPokeB == 0 && InvPokeGB == 0 && InvPokeUB >= 1)
	{
		cout << endl << "Choose a ball: " << endl << "[U] - Ultra ball" << endl;
		cin >> Action;
		if (Action == "U")
		{
			cout << endl << "You threw an Ultra ball" << endl;
			InvPokeUB--;
			Chance += 30;
		}
	}

	else if (InvPokeB == 0 && InvPokeGB >= 1 && InvPokeUB >= 1)
	{
		cout << endl << "Choose a ball: " << endl << "[G] - Great ball" << endl << "[U] - Ultra ball" << endl;
		cin >> Action;
		if (Action == "G")
		{
			cout << endl << "You threw a Great ball" << endl;
			InvPokeGB--;
			Chance += 20;
		}
		if (Action == "U")
		{
			cout << endl << "You threw an Ultra ball" << endl;
			InvPokeUB--;
			Chance += 30;
		}
	}
	else if (InvPokeB >= 1 && InvPokeGB == 0 && InvPokeUB >= 1)
	{
		cout << endl << "Choose a ball: " << endl << "[P] - Pokeball" << endl << "[U] - Ultra ball" << endl;
		cin >> Action;
		if (Action == "P")
		{
			cout << endl << "You threw a Pokeball" << endl;
			InvPokeB--;
			Chance += 10;
		}
		if (Action == "U")
		{
			cout << endl << "You threw an Ultra ball" << endl;
			InvPokeUB--;
			Chance += 30;
		}
	}
	else if (InvPokeB == 0 && InvPokeGB >= 1 && InvPokeUB >= 1)
	{
		cout << endl << "Choose a ball: " << endl << endl << "[G] - Great ball" << endl << "[U] - Ultra ball" << endl;
		cin >> Action;
		if (Action == "G")
		{
			cout << endl << "You threw a Great ball" << endl;
			InvPokeGB--;
			Chance += 20;
		}
		if (Action == "U")
		{
			cout << endl << "You threw an Ultra ball" << endl;
			InvPokeUB--;
			Chance += 30;
		}
	}
}

void Catching(WildPokemon * pokemon, string rarity, vector<WildPokemon> WildPokemonsList)
{
	system("cls");
	if (rarity == "Very Common")
	{
		Chance = 40;
	}
	else if (rarity == "Common")
	{
		Chance = 30;
	}
	else if (rarity == "Rare")
	{
		Chance = 20;
	}
	else if (rarity == "Very Rare")
	{
		Chance = 10;
	}
	cout << "Encountered a " << rarity << " Pokemon" << endl << endl;
	cout << "A wild " << pokemon->NAME << " appeared!" << endl << endl;
	cout << "----------------------" << endl;
	cout << pokemon->NAME << " Status:" << endl;
	cout << "Level: " << pokemon->LVL << endl;
	cout << "HP: " << pokemon->CURHP << "/" << pokemon->MAXHP << endl;
	cout << "Damage: " << pokemon->ATK << endl;
	cout << "----------------------" << endl;
	cout << " " << endl;

	WildPokemon CurrentPokemon = *myPokemons[0];

	while (true)
	{
		while (CurrentPokemon.CURHP > 0 && pokemon > 0)
		{
			cout << "---------------------------------------------------------" << endl;
			cout << "What do you want to do?" << endl << endl << "[F] - Fight" << endl << "[C] - Catch" << endl << "[R] - Run Away" << endl;
			cout << "---------------------------------------------------------" << endl;
			cin >> Action;
			if (Action == "F")
			{
				//this is to check which has higher DEX = who starts the battle first
				//if PlayersPokemons DEX is higher than WildPokemon
				if (CurrentPokemon.CURHP <= 0)
				{
					cout << "Your Pokemons can not fight anymore!" << endl;
					break;
				}
				else if (CurrentPokemon.DEX >= pokemon->DEX)
				{
					while (CurrentPokemon.CURHP > 0 && pokemon->CURHP > 0)
					{
						cout << "----------------------" << endl;
						cout << "Your " << CurrentPokemon.NAME << " has attacked " << pokemon << "!" << endl;
						if (CurrentPokemon.ELEMENT == "Ground" && pokemon->ELEMENT == "Poison" || pokemon->ELEMENT == "Rock" || pokemon->ELEMENT == "Fire")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Fire" && pokemon->ELEMENT == "Bug" || pokemon->ELEMENT == "Grass")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Water" && pokemon->ELEMENT == "Ground" || pokemon->ELEMENT == "Rock" || pokemon->ELEMENT == "Fire")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Bug" && pokemon->ELEMENT == "Grass" || pokemon->ELEMENT == "Psychic")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Psychic" && pokemon->ELEMENT == "Poison")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Flying" && pokemon->ELEMENT == "Bug" || pokemon->ELEMENT == "Grass")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Rock" && pokemon->ELEMENT == "Flying" || pokemon->ELEMENT == "Bug" || pokemon->ELEMENT == "Fire")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Dragon" && pokemon->ELEMENT == "Dragon")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Grass" && pokemon->ELEMENT == "Water" || pokemon->ELEMENT == "Ground" || pokemon->ELEMENT == "Rock")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Ghost" && pokemon->ELEMENT == "Ghost" || pokemon->ELEMENT == "Psychic")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else
						{
							pokemon->CURHP = pokemon->CURHP - CurrentPokemon.ATK;
						}
						cout << "----------------------" << endl;
						cout << pokemon->NAME << ": " << pokemon->CURHP << "/" << pokemon->MAXHP << endl;
						system("pause");
						if (pokemon->CURHP <= 0)
						{
							break;
						}
						cout << "----------------------" << endl;
						CurrentPokemon.CURHP = CurrentPokemon.CURHP - pokemon->ATK;
						cout << pokemon->NAME << " has attacked your " << CurrentPokemon.NAME << "!" << endl;
						cout << "----------------------" << endl;
						cout << CurrentPokemon.NAME << ": " << CurrentPokemon.CURHP << "/" << CurrentPokemon.MAXHP << endl;
						system("pause");
						if (CurrentPokemon.CURHP <= 0)
						{
							break;
						}
					}
					if (CurrentPokemon.CURHP <= 0)
					{
						cout << "You have been defeated" << endl;
						break;
					}
					else if (pokemon->CURHP <= 0)
					{
						cout << "You won against " << pokemon->NAME << "!" << endl;
						cout << CurrentPokemon.NAME << " earned " << pokemon->XPTOGIVE << " xp!" << endl;
						CurrentPokemon.XP = CurrentPokemon.XP + pokemon->XPTOGIVE;
						cout << CurrentPokemon.NAME << ": " << CurrentPokemon.XP << "/" << CurrentPokemon.XPTOLVLUP << endl;
						cout << "----------------------" << endl;
						cout << "You earned 150P!" << endl;
						MONEY = MONEY + 150;
						system("pause");
						system("cls");


						//TESTING IF LEVELING UP SYSTEM WORKS HERE//
						if (CurrentPokemon.XP >= CurrentPokemon.XPTOLVLUP)
						{
							CurrentPokemon.LVL++;
							CurrentPokemon.MAXHP = CurrentPokemon.MAXHP + (CurrentPokemon.MAXHP * 0.15);
							CurrentPokemon.ATK = CurrentPokemon.ATK + (CurrentPokemon.ATK * 0.1);
							CurrentPokemon.XPTOLVLUP = CurrentPokemon.XPTOLVLUP + (CurrentPokemon.XPTOLVLUP * 0.2);

							if (CurrentPokemon.LVL >= 30 && CurrentPokemon.EVOLVE == 3 && CurrentPokemon.CURREV == 2)
							{
								CurrentPokemon.POKEDEXNUM++;
								for (size_t i = 0; i < WildPokemonsList.size(); i++)
								{
									if (CurrentPokemon.POKEDEXNUM == WildPokemonsList[i].POKEDEXNUM)
									{
										CurrentPokemon.NAME = WildPokemonsList[i].NAME;
									}
								}
							}
							else if (CurrentPokemon.LVL >= 20 && CurrentPokemon.EVOLVE == 2 && CurrentPokemon.CURREV == 1)
							{
								CurrentPokemon.POKEDEXNUM++;
								for (size_t i = 0; i < WildPokemonsList.size(); i++)
								{
									if (CurrentPokemon.POKEDEXNUM == WildPokemonsList[i].POKEDEXNUM)
									{
										CurrentPokemon.NAME = WildPokemonsList[i].NAME;
									}
								}
							}
							else if (CurrentPokemon.LVL >= 40 && CurrentPokemon.EVOLVE == 4 && CurrentPokemon.CURREV == 3)
							{
								CurrentPokemon.POKEDEXNUM++;
								for (size_t i = 0; i < WildPokemonsList.size(); i + 100)
								{
									if (CurrentPokemon.POKEDEXNUM == WildPokemonsList[i].POKEDEXNUM)
									{
										CurrentPokemon.NAME = WildPokemonsList[i].NAME;
									}
								}
							}

						}
						//TESTING IF LEVELING UP SYSTEM WORKS HERE//


						//Refresh the wildpokemon's HP now so it wont affect later on's WildPokemons.
						pokemon->CURHP = pokemon->MAXHP;
						//insert give exp function here
						return;
					}
				}
				//if WildPokemon DEX is higher than PlayersPokemon
				else if (CurrentPokemon.DEX <= pokemon->DEX)
				{
					while (CurrentPokemon.CURHP > 0 || pokemon->CURHP > 0)
					{
						CurrentPokemon.CURHP = CurrentPokemon.CURHP - pokemon->ATK;
						cout << pokemon->NAME << " has attacked your " << CurrentPokemon.NAME << "!" << endl;
						cout << "----------------------" << endl;
						cout << CurrentPokemon.NAME << ": " << CurrentPokemon.CURHP << "/" << CurrentPokemon.MAXHP << endl;
						system("pause");
						if (CurrentPokemon.CURHP <= 0)
						{
							break;
						}
						cout << "----------------------" << endl;
						cout << "Your " << CurrentPokemon.NAME << " has attacked " << pokemon << "!" << endl;
						if (CurrentPokemon.ELEMENT == "Ground" && pokemon->ELEMENT == "Poison" || pokemon->ELEMENT == "Rock" || pokemon->ELEMENT == "Fire")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Fire" && pokemon->ELEMENT == "Bug" || pokemon->ELEMENT == "Grass")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Water" && pokemon->ELEMENT == "Ground" || pokemon->ELEMENT == "Rock" || pokemon->ELEMENT == "Fire")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Bug" && pokemon->ELEMENT == "Grass" || pokemon->ELEMENT == "Psychic")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Psychic" && pokemon->ELEMENT == "Poison")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Flying" && pokemon->ELEMENT == "Bug" || pokemon->ELEMENT == "Grass")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Rock" && pokemon->ELEMENT == "Flying" || pokemon->ELEMENT == "Bug" || pokemon->ELEMENT == "Fire")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Dragon" && pokemon->ELEMENT == "Dragon")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Grass" && pokemon->ELEMENT == "Water" || pokemon->ELEMENT == "Ground" || pokemon->ELEMENT == "Rock")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else if (CurrentPokemon.ELEMENT == "Ghost" && pokemon->ELEMENT == "Ghost" || pokemon->ELEMENT == "Psychic")
						{
							cout << "It was super effective!" << endl;
							pokemon->CURHP = (pokemon->CURHP) - (CurrentPokemon.ATK) + (CurrentPokemon.ATK / 2);
						}
						else
						{
							pokemon->CURHP = pokemon->CURHP - CurrentPokemon.ATK;
						}
						cout << pokemon->NAME << ": " << pokemon->CURHP << "/" << pokemon->MAXHP << endl;
						system("pause");
						if (pokemon->CURHP <= 0)
						{
							break;
						}
					}
					if (CurrentPokemon.CURHP <= 0)
					{
						cout << "You have been defeated" << endl;
						break;
					}
					else if (pokemon->CURHP <= 0)
					{
						cout << "You won against " << pokemon->NAME << "!" << endl;
						cout << CurrentPokemon.NAME << " earned " << pokemon->XPTOGIVE << " xp!" << endl;
						CurrentPokemon.XP = CurrentPokemon.XP + pokemon->XPTOGIVE;
						cout << CurrentPokemon.NAME << ": " << CurrentPokemon.XP << "/" << CurrentPokemon.XPTOLVLUP << endl;
						cout << "----------------------" << endl;
						cout << "You earned 150P!" << endl;
						MONEY = MONEY + 150;
						system("pause");
						system("cls");


						//TESTING IF LEVELING UP SYSTEM WORKS HERE//
						if (CurrentPokemon.XP >= CurrentPokemon.XPTOLVLUP)
						{
							CurrentPokemon.LVL++;
							CurrentPokemon.MAXHP = CurrentPokemon.MAXHP + (CurrentPokemon.MAXHP * 0.15);
							CurrentPokemon.ATK = CurrentPokemon.ATK + (CurrentPokemon.ATK * 0.1);
							CurrentPokemon.XPTOLVLUP = CurrentPokemon.XPTOLVLUP + (CurrentPokemon.XPTOLVLUP * 0.2);

							if (CurrentPokemon.LVL >= 30 && CurrentPokemon.EVOLVE == 3 && CurrentPokemon.CURREV == 2)
							{
								CurrentPokemon.POKEDEXNUM++;
								for (size_t i = 0; i < WildPokemonsList.size(); i++)
								{
									if (CurrentPokemon.POKEDEXNUM == WildPokemonsList[i].POKEDEXNUM)
									{
										CurrentPokemon.NAME = WildPokemonsList[i].NAME;
									}
								}
							}
							else if (CurrentPokemon.LVL >= 20 && CurrentPokemon.EVOLVE == 2 && CurrentPokemon.CURREV == 1)
							{
								CurrentPokemon.POKEDEXNUM++;
								for (size_t i = 0; i < WildPokemonsList.size(); i++)
								{
									if (CurrentPokemon.POKEDEXNUM == WildPokemonsList[i].POKEDEXNUM)
									{
										CurrentPokemon.NAME = WildPokemonsList[i].NAME;
									}
								}
							}
							else if (CurrentPokemon.LVL >= 40 && CurrentPokemon.EVOLVE == 4 && CurrentPokemon.CURREV == 3)
							{
								CurrentPokemon.POKEDEXNUM++;
								for (size_t i = 0; i < WildPokemonsList.size(); i++)
								{
									if (CurrentPokemon.POKEDEXNUM == WildPokemonsList[i].POKEDEXNUM)
									{
										CurrentPokemon.NAME = WildPokemonsList[i].NAME;
									}
								}
							}

						}
						//TESTING IF LEVELING UP SYSTEM WORKS HERE//


						//Refresh the wildpokemon's HP now so it wont affect later on's WildPokemons.
						pokemon->CURHP = pokemon->MAXHP;
						//insert give exp function here
						return;
					}
				}

			}
			if (Action == "C")
			{
				if (InvPokeB >= 1 || InvPokeGB >= 1 || InvPokeUB >= 1)
				{
					ChoosePokeball();
					int random = (rand() % 100) + 1;
					system("pause");
					cout << "." << endl;
					system("pause");
					cout << ".." << endl;
					system("pause");
					cout << "..." << endl;
					system("pause");
					if (random <= Chance)
					{
						cout << "Congratulations! " << pokemon->NAME << " has been caught!" << endl;
						system("pause");
						myPokemons.push_back(pokemon);
						return;
					}
					else
					{
						cout << pokemon->NAME << " got out of the pokeball!" << endl << endl;
						system("pause");
						int flee = (rand() % 100) + 1;
						if (rarity == "Very Common")
						{
							if (flee <= 5)
							{
								cout << pokemon->NAME << " has fled." << endl;
								system("pause");
								//Refresh the wildpokemon's HP now so it wont affect later on's WildPokemons.
								pokemon->CURHP = pokemon->MAXHP;
								return;
							}
						}
						else if (rarity == "Common")
						{
							if (flee <= 10)
							{
								cout << pokemon->NAME << " has fled." << endl;
								system("pause");
								//Refresh the wildpokemon's HP now so it wont affect later on's WildPokemons.
								pokemon->CURHP = pokemon->MAXHP;
								return;
							}
						}
						else if (rarity == "Rare")
						{
							if (flee <= 30)
							{
								cout << pokemon->NAME << " has fled." << endl;
								system("pause");
								//Refresh the wildpokemon's HP now so it wont affect later on's WildPokemons.
								pokemon->CURHP = pokemon->MAXHP;
								return;
							}
						}
						else if (rarity == "Very Rare")
						{
							if (flee <= 80)
							{
								cout << pokemon->NAME << " has fled." << endl;
								system("pause");
								//Refresh the wildpokemon's HP now so it wont affect later on's WildPokemons.
								pokemon->CURHP = pokemon->MAXHP;
								return;
							}
						}
						system("cls");
						//Refresh the wildpokemon's HP now so it wont affect later on's WildPokemons.
						pokemon->CURHP = pokemon->MAXHP;
					}
				}
				else
				{
					cout << "You do not have any of the PokemonBalls to catch them!" << endl;
					system("pause");
					system("cls");
				}
			}
			if (Action == "R")
			{
				cout << "You successfully escaped!" << endl;
				//Refresh the wildpokemon's HP now so it wont affect later on's WildPokemons.
				pokemon->CURHP = pokemon->MAXHP;
				return;
			}
		}
		//THIS IS SO THAT PLAYERS POKEMON WILL SWAP IF ONE FAINTS
		if (CurrentPokemon.CURHP <= 0)
		{
			for (size_t i = 0; i < myPokemons.size(); i++)
			{
				if (myPokemons[i]->CURHP > 0)
				{
					CurrentPokemon = *myPokemons[i];
					break;
				}
				myPokemons[i]->CURHP = 0;
			}
		}
		if (CurrentPokemon.CURHP <= 0 || pokemon->CURHP <= 0)
		{
			break;
		}
	}
}

void playermove()
{
	cout << "Where do you want to move?" << endl;
	cout << "[W] - Up   [S] - Down   [A] - Left   [D] - Right" << endl;
	cin >> playerpos;
	while (true)
	{
		if (playerpos == "W" || playerpos == "w")
		{
			playerx++;
			break;
		}
		else if (playerpos == "S" || playerpos == "s")
		{
			playerx--;
			break;
		}
		else if (playerpos == "A" || playerpos == "a")
		{
			playerz--;
			break;
		}
		else if (playerpos == "D" || playerpos == "d")
		{
			playerz++;
			break;
		}
		else
		{
			cout << "Invalid Option";
		}
	}
}