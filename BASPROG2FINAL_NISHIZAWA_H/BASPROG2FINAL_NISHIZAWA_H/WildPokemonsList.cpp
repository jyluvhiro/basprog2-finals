#include <string>
#include <iostream>
#include <conio.h>
#include <time.h>
#include <conio.h>
#include <vector>
#include "pch.h"

#include "WildPokemonsList.h"

using namespace std;

PokeDex::PokeDex()
{
	//THIS ARE THE LIST OF POKEMONS IN THE GAME OR COULD BE BY LEVLING UP, 5 MEGA POKEMONS INCLUDED. theyre too op tbh i really wanted to add more

	//                                     ("NAME", "ELEMENT", HP, HP, ATK, DEX, LVL, XP,LVLUP, CUREV, EVOLVE, POKEDEX, XPTOGIVE)
	WildPokemon * _001 = new WildPokemon("Bulbasaur", "Grass", 45, 45, 16, 45, 1, 0, 39, 1, 4, 001, 14);
	WildPokemon * _002 = new WildPokemon("Ivysaur", "Grass", 173, 173, 46, 60, 20, 0, 187, 2, 4, 002, 67);
	WildPokemon * _003 = new WildPokemon("Venusaur", "Grass", 407, 407, 134, 80, 30, 0, 524, 3, 4, 003, 188);
	WildPokemon * _103 = new WildPokemon("Mega Venusaur", "Grass", 956, 956, 255, 80, 40, 0, 1467, 4, 4, 103, 188);
	//                                     ("NAME", "ELEMENT", HP, HP, ATK, DEX, LVL, XP,LVLUP, CUREV, EVOLVE, POKEDEX, XPTOGIVE)
	WildPokemon * _004 = new WildPokemon("Charmander", "Fire", 39, 39, 17, 65, 1, 0, 39, 1, 4, 004, 14);
	WildPokemon * _005 = new WildPokemon("Charmeleon", "Fire", 150, 150, 46, 80, 1, 0, 187, 2, 4, 005, 67);
	WildPokemon * _006 = new WildPokemon("Charizard", "Fire", 352, 352, 134, 100, 1, 0, 524, 3, 4, 006, 188);
	WildPokemon * _106 = new WildPokemon("Mega Charizard", "Fire", 829, 829, 271, 70, 40, 0, 1467, 4, 4, 106, 188);
	//                                     ("NAME", "ELEMENT", HP, HP, ATK, DEX, LVL, XP,LVLUP,EV POKEDEX, XPTOGIVE)
	WildPokemon * _807 = new WildPokemon("Squirtle", "Water", 44, 44, 16, 43, 1, 0, 39, 1, 3, 807, 14);
	WildPokemon * _808 = new WildPokemon("Wartotle", "Water", 169, 63, 46, 58, 20, 0, 187, 2, 3, 808, 67);
	WildPokemon * _809 = new WildPokemon("Blastoise", "Water", 398, 63, 134, 39, 30, 0, 524, 3, 3, 809, 188);
	WildPokemon * _810 = new WildPokemon("Mega Blastoise", "Water", 902, 902, 223, 80, 40, 0, 1467, 4, 4, 909, 188);
	//                                      ("NAME", "ELEMENT", HP, HP, ATK, DEX, LVL, XP,LVLUP,EV POKEDEX, XPTOGIVE)
	WildPokemon * _010 = new WildPokemon("Caterpie", "Bug", 45, 45, 10, 45, 1, 0, 39, 1, 3, 010, 14);
	WildPokemon * _011 = new WildPokemon("Metapod", "Bug", 173, 173, 29, 30, 20, 0, 187, 2, 3, 011, 67);
	WildPokemon * _012 = new WildPokemon("Butterfree", "Bug", 398, 84, 134, 70, 30, 0, 524, 3, 3, 012, 188);
	//                                     ("NAME", "ELEMENT", HP, HP, ATK, DEX, LVL, XP,LVLUP,EV POKEDEX, XPTOGIVE)
	WildPokemon * _013 = new WildPokemon("Weedle", "Bug", 40, 40, 10, 50, 1, 0, 39, 1, 3, 013, 14);
	WildPokemon * _014 = new WildPokemon("Kakuna", "Bug", 154, 154, 29, 40, 20, 0, 187, 2, 3, 014, 67);
	WildPokemon * _015 = new WildPokemon("Beedrill", "Poison", 360, 360, 134, 75, 30, 0, 524, 3, 3, 015, 188);
	//                                     ("NAME", "ELEMENT", HP, HP, ATK, DEX, LVL, XP,LVLUP,EV POKEDEX, XPTOGIVE)
	WildPokemon * _023 = new WildPokemon("Ekans", "Poison", 35, 35, 30, 55, 1, 0, 39, 1, 2, 023, 14);
	WildPokemon * _024 = new WildPokemon("Arbok", "Poison", 134, 134, 87, 55, 20, 0, 187, 2, 2, 024, 67);
	//                                       ("NAME", "ELEMENT", HP, HP, ATK, DEX, LVL, XP,LVLUP,EV POKEDEX, XPTOGIVE)
	WildPokemon * _816 = new WildPokemon("Pidgey", "Flying", 40, 40, 15, 56, 1, 0, 39, 1, 2, 016, 14);
	WildPokemon * _817 = new WildPokemon("Pidgeotto", "Flying", 154, 154, 43, 40, 20, 0, 187, 2, 2, 017, 67);
	WildPokemon * _818 = new WildPokemon("Pidgeot", "Flying", 360, 360, 126, 75, 30, 0, 524, 3, 3, 818, 188);
	//                                       ("NAME", "ELEMENT", HP, HP, ATK, DEX, LVL, XP,LVLUP,EV POKEDEX, XPTOGIVE)
	WildPokemon * _819 = new WildPokemon("Rattata", "Normal", 30, 30, 14, 52, 1, 0, 39, 1, 2, 819, 14);
	WildPokemon * _820 = new WildPokemon("Raticate", "Normal", 115, 115, 40, 40, 20, 0, 187, 2, 2, 820, 67);
	//
	WildPokemon * _021 = new WildPokemon("Spearow", "Flying", 40, 40, 15, 56, 1, 0, 39, 1, 2, 021, 14);
	WildPokemon * _022 = new WildPokemon("Fearow", "Flying", 154, 154, 43, 40, 20, 0, 187, 2, 2, 022, 67);
	//                                      ("NAME", "ELEMENT", HP, HP, ATK, DEX, LVL, XP,LVLUP,EV POKEDEX, XPTOGIVE)
	WildPokemon * _025 = new WildPokemon("Pikachu", "Electric", 35, 35, 18, 90, 1, 0, 39, 1, 2, 025, 14);
	WildPokemon * _026 = new WildPokemon("Raichu", "Flying", 134, 134, 53, 110, 20, 0, 187, 2, 2, 026, 67);

	WildPokemon * _074 = new WildPokemon("Geodude", "Rock", 45, 45, 16, 45, 1, 0, 39, 1, 4, 074, 14);
	WildPokemon * _075 = new WildPokemon("Graveler", "Rock", 173, 173, 46, 60, 20, 0, 187, 2, 4, 075, 67);
	WildPokemon * _076 = new WildPokemon("Golem", "Rock", 407, 407, 134, 80, 30, 0, 524, 3, 4, 076, 188);

	WildPokemon * _892 = new WildPokemon("Gastly", "Ghost", 40, 40, 10, 50, 1, 0, 39, 1, 4, 892, 14);
	WildPokemon * _893 = new WildPokemon("Haunter", "Ghost", 154, 154, 29, 40, 20, 0, 187, 2, 4, 893, 67);
	WildPokemon * _894 = new WildPokemon("Gengar", "Ghost", 360, 360, 134, 75, 30, 0, 524, 3, 4, 894, 188);
	WildPokemon * _994 = new WildPokemon("Mega Gengar", "Ghost", 956, 956, 255, 80, 40, 0, 1467, 4, 4, 994, 188);
	//                                      ("NAME", "ELEMENT", HP, HP, ATK, DEX, LVL, XP,LVLUP,EV POKEDEX, XPTOGIVE)
	WildPokemon * _149 = new WildPokemon("Dragonite", "Dragon", 91, 91, 44, 80, 1, 0, 187, 1, 1, 149, 367);
	//                                      ("NAME", "ELEMENT", HP, HP, ATK, DEX, LVL, XP,LVLUP,EV POKEDEX, XPTOGIVE)
	WildPokemon * _150 = new WildPokemon("Mew Two", "Psychic", 106, 106, 36, 130, 1, 0, 187, 1, 1, 150, 467);
	//                                      ("NAME", "ELEMENT", HP, HP, ATK, DEX, LVL, XP,LVLUP,EV POKEDEX, XPTOGIVE)
	WildPokemon * _132 = new WildPokemon("Ditto", "Normal", 48, 48, 48, 48, 1, 0, 187, 1, 1, 132, 467);

	WildPokemon * _115 = new WildPokemon("Kangaskhan", "Dragon", 404, 404, 31, 25, 1, 0, 187, 3, 4, 115, 367);
	WildPokemon * _215 = new WildPokemon("Mega Venusaur", "Grass", 1256, 1256, 216, 45, 40, 0, 1467, 4, 4, 215, 788);


	//VeryCommon Pokemons(_001);
	VeryCommon.push_back(_010); //[0] is CATA
	VeryCommon.push_back(_013); //[1] is WEEDLE
	VeryCommon.push_back(_816); //[2] is PIDGEY
	VeryCommon.push_back(_819); //[3] is RATTATA
	VeryCommon.push_back(_021); //[4] is SPEAROW
	//Common Pokemons
	Common.push_back(_001); //[0] is BULB
	Common.push_back(_004); //[1] is CHAR
	Common.push_back(_807); //[2] is SQUA
	Common.push_back(_023); //[3] is EKANS
	Common.push_back(_074); //[4] IS GEODUDE
	Common.push_back(_892); //[5]
	//Rare Pokemons
	Rare.push_back(_025); //[0] IS PIKACHU
	Rare.push_back(_894); //[1] GENGAR
	Rare.push_back(_132); //[2] Dittoooooooooooooooo
	Rare.push_back(_115); //[3] Kangas
	//VeryRare Pokemons
	VeryRare.push_back(_150); //[0] IS MEWTWO
	VeryRare.push_back(_149); //[1] IS DRAGONITE
}