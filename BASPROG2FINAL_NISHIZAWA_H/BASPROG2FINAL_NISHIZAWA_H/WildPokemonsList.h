#pragma once
#include <string>
#include <iostream>
#include <conio.h>
#include <time.h>
#include <conio.h>
#include <vector>

#include "WildPokemons.h"

using namespace std;

class PokeDex
{
public:
	PokeDex();
	vector<WildPokemon *> VeryCommon;
	vector<WildPokemon *> Common;
	vector<WildPokemon *> Rare;
	vector<WildPokemon *> VeryRare;
};