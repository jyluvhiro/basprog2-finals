#include <string>
#include <iostream>
#include <conio.h>
#include <time.h>
#include <conio.h>
#include <vector>
#include "pch.h"


#include "WildPokemons.h"

WildPokemon::WildPokemon(string NAME, string ELEMENT, int MAXHP, int CURHP, int ATK, int DEX, int LVL, int XP, int XPTOLVLUP, int CURREV, int EVOLVE, int POKEDEXNUM, int XPTOGIVE)
{
	this->NAME = NAME; //the first name is whatever is your variable name in your .h . // the second name is what you wrote one top here 
	this->ELEMENT = ELEMENT;
	this->MAXHP = MAXHP;
	this->CURHP = CURHP;
	this->ATK = ATK;
	this->DEX = DEX;
	this->LVL = LVL;
	this->XP = XP;
	this->XPTOLVLUP = XPTOLVLUP;
	this->CURREV = CURREV;
	this->EVOLVE = EVOLVE;
	this->POKEDEXNUM = POKEDEXNUM;
	this->XPTOGIVE = XPTOGIVE;
}