//BASPROG2 FINALS: ADVANCED POKEMON SIMULATOR: HIROMICHI NISHIZAWA 11815774

//Dear Sir
//I really tried my best, I could find some glitches but I had no idea how to fix them...
//Codes itself is working I checked, though some of the stuff that will be shown to the Player is glitching
//Im truly sorry for whatever I have not submitted, I am just bad at coding.
//Thank you so much for teaching all of us this term. I had fun, I started seeing Coding is fun when it works,
//But I still ended up like this. I hope I can do it better in the next basprog

#include <string>
#include <iostream>
#include <conio.h>
#include <time.h>
#include <conio.h>
#include <vector>
#include "pch.h"

#include "WildPokemonsList.h"
#include "FirstScene.h"
#include "EncounteringandCatching.h"
#include "Townstuff.h"

using namespace std;

int main()
{
	PokeDex* PokiPokie = new PokeDex();
	firstscene(PokiPokie);

	while (true)
	{
		system("cls");
		cout << "What would you like to do?" << endl;
		//This is to see if Player is within range of Pallet Town. -2~2, or 0.
		if (playerx >= -2 && playerz >= -2 && playerx <= 2 && playerz <= 2)
		{
			cout << "[1] - Move   [2] - Bag&Pokemons   [3] - Pallet Town   [4] - Potion" << endl;
		}
		else
		{
			cout << "[1] - Move   [2] - Bag&Pokemons   [4] - Potion" << endl;
		}
		string playerdecision;
		cin >> playerdecision;
		//Player choose to Wander around outside
		if (playerdecision == "1")
		{
			cout << "-------------------------------------------------" << endl;
			playermove();
			cout << "-------------------------------------------------" << endl;
			//This is to see if Player is within range of Pallet Town. -2~2, or 0.
			if (playerx >= -2 && playerz >= -2 && playerx <= 2 && playerz <= 2)
			{
				cout << R"(
~         ~~          __
       _T      .,,.    ~--~ ^^
 ^^   // \                    ~
      ][O]    ^^      ,-~ ~
   /''-I_I         _II____
__/_  /   \ ______/ ''   /'\_,__
  | II--'''' \,--:--..,_/,.-{ },
; '/__\,.--';|   |[] .-.| O{ _ }
:' |  | []  -|   ''--:.;[,.'\,/
'  |[]|,.--'' '',   ''-,.    |
  ..    ..-''    ;       ''. '
                           )" << '\n';
				cout << "You are now at position (" << playerx << ", " << playerz << ")" << endl;
				cout << "You have entered Pallet Town" << endl;
			}
			else
			{
				wildpokemonencounter(PokiPokie);
				cout << "You are now at position (" << playerx << ", " << playerz << ")" << endl;
			}
			system("pause");
			system("cls");
		}
		//Player choose to check his Pokemons.
		else if (playerdecision == "2")
		{
			cout << "You checked your Bag and Pokemons" << endl;
			cout << "-------------------------------------------------" << endl;
			cout << "You currently have: " << endl;
			cout << "P" << MONEY << "   " << InvPokeB << " PokemonBall   " << InvPokeGB << " GreatBall   " << InvPokeUB << " UltraBall   " << InvPotion << " Potions" << endl;
			cout << "----------------------" << endl;
			checkpokemon();
			system("pause");
		}
		//Player choose to Wander around in the Pallet Town
		else if (playerdecision == "3")
		{
			cout << R"(
                   _            ______    _   ________     _
    ____________ .' '.    _____/----/-\ .' './========\   / \
   //// ////// /V_.-._\  |.-.-.|===| _ |-----| u    u |  /___\
  // /// // ///==\ u |.  || | ||===||||| |T| |   ||   | .| u |_ _ _ _ _ _
 ///////-\////====\==|:::::::::::::::::::::::::::::::::::|u u| U U U U U
 |----/\u |--|++++|..|'''''''''''::::::::::::::''''''''''|+++|+-+-+-+-+-+
 |u u|u | |u ||||||..|              '::::::::'           |===|>=== _ _ ==
 |===|  |u|==|++++|==|              .::::::::.           | T |....| V |..
 |u u|u | |u ||HH||         \|/    .::::::::::.
 |===|_.|u|_.|+HH+|_              .::::::::::::.              _
                __(_)___         .::::::::::::::.         ___(_)__
---------------/  / \  /|       .:::::;;;:::;;:::.       |\  / \  \-------
______________/_______/ |      .::::::;;:::::;;:::.      | \_______\________
|       |     [===  =] /|     .:::::;;;::::::;;;:::.     |\ [==  = ]   |
|_______|_____[ = == ]/ |    .:::::;;;:::::::;;;::::.    | \[ ===  ]___|____
     |       |[  === ] /|   .:::::;;;::::::::;;;:::::.   |\ [=  ===] |
_____|_______|[== = =]/ |  .:::::;;;::::::::::;;;:::::.  | \[ ==  =]_|______
                       )" << '\n';

			cout << "You're now wandering Pallet Town" << endl;
			cout << "-------------------------------------------------" << endl;
			cout << "Where would you like to visit?" << endl;
			cout << "[1] Healing Center   [2] General Shop" << endl;
			string towndecision;
			cin >> towndecision;
			if (towndecision == "1")
			{
				system("cls");
				cout << "-------------------------------------------------" << endl;
				cout << "Welcome to the Pallet Town Pokemon Center!" << endl;
				cout << "Would you like me to heal your Pokemon back to perfect health?" << endl;
				system("pause");
				//Typing down Y or y gets to heal, N or n for not.
				cout << "[Y] Yes   [N] Not now" << endl;
				char response;
				cin >> response;
				if (response == 'Y' || response == 'y')
				{
					system("cls");
					cout << "*Ten Ten Tenenenen" << endl;
					system("pause");
					townhealpokemon();
					system("cls");
					cout << "Your Pokemons are now in perfect health" << endl;
				}
				else if (response == 'N' || response == 'n')
				{
					system("cls");
					cout << "Oh Okay!" << endl;
				}
				cout << "We hope to see you again!" << endl;
			}
			else if (towndecision == "2")
			{
				system("cls");
				cout << "You currently have: P" << MONEY << endl;
				cout << "-------------------------------------------------" << endl;
				cout << "Welcome to the Pallet Town General Shop!" << endl;
				PokeMart();
			}
			system("pause");
		}
		else if (playerdecision == "4")
		{
			if (InvPotion <= 0)
			{
				cout << "You do not have any Potions to heal your pokemon!" << endl;
				system("pause");
			}
			else if (InvPotion > 0)
			{
				cout << "You have " << InvPotion << " Potions in your bag!" << endl;
				cout << "Would you like to let your Pokemons drink it?" << endl;
				string potiondecision;
				cout << "[1] - Yes   [2] - No" << endl;
				cin >> potiondecision;
				if (potiondecision == "1")
				{
					system("cls");
					cout << "You used Potion!" << endl;
					InvPotion--;
					potionhealpokemon();
					system("pause");
				}
				else if (potiondecision == "2")
				{
					system("cls");
					cout << "You didn't use Potion!" << endl;
					system("pause");
				}
			}

		}
		else
		{
			cout << "Invalid Option" << endl;
			cout << "-------------------------------------------------" << endl;
			system("pause");
			system("cls");
		}
	}
}
